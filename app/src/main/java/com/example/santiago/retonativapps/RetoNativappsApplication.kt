package com.example.santiago.retonativapps

import com.example.santiago.retonativapps.di.AppComponent
import com.example.santiago.retonativapps.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

class RetoNativappsApplication : DaggerApplication() {

    var mAppComponent: AppComponent? = null

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        val component = DaggerAppComponent.builder()
            .application(this)
            .build()
        mAppComponent = component
        component.inject(this)
        return component
    }

}
