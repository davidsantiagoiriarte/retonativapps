package com.example.santiago.retonativapps.data.models

data class APIMoviesResponse(
    val Search: List<Search>?,
    val totalResults: String?,
    val Response: String?
)
