package com.example.santiago.retonativapps.data.models

data class Search(
    val Title: String?,
    val Year: String?,
    val imdbID: String?,
    val Type: String?,
    val Poster: String?
)
