package com.example.santiago.retonativapps.data.network

import com.example.santiago.retonativapps.data.models.APIMoviesResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface MoviesService {

    @GET(".")
    fun getMoviesService(@Query("apikey") apikey: String, @Query("s") search: String): Observable<APIMoviesResponse>
}
