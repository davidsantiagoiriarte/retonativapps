package com.example.santiago.retonativapps.data.repositories

import com.example.santiago.retonativapps.data.network.MoviesService
import com.example.santiago.retonativapps.data.network.utils.API_KEY
import com.example.santiago.retonativapps.data.network.utils.MAX_RESULTS
import com.example.santiago.retonativapps.domain.models.Movie
import com.example.santiago.retonativapps.domain.repositories.IMoviesRepository
import io.reactivex.Observable

class MoviesRepository(private val mService: MoviesService) : IMoviesRepository {

    override fun getMovies(search: String): Observable<List<Movie>> {
        return mService.getMoviesService(API_KEY, search)?.map {
            it.Search?.take(MAX_RESULTS)?.map {
                Movie(it.Title, it.Year, it.imdbID, it.Type, it.Poster)
            }
        }
    }

}
