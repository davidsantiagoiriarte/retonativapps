package com.example.santiago.retonativapps.di

import com.example.santiago.retonativapps.mainactivity.MainActivity
import com.example.santiago.retonativapps.mainactivity.MainActivityModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [(MainActivityModule::class)])
    abstract fun bindMainActivity(): MainActivity

}
