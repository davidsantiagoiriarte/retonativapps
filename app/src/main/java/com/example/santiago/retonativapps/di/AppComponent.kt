package com.example.santiago.retonativapps.di

import android.app.Application
import com.example.santiago.retonativapps.RetoNativappsApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [(AndroidSupportInjectionModule::class),
        (AppModule::class),
        (ActivityBuilder::class),
        (InteractorsModule::class),
        (RepositoryModule::class)]
)
interface AppComponent : AndroidInjector<DaggerApplication> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(retoNativappsApplication: RetoNativappsApplication)

}
