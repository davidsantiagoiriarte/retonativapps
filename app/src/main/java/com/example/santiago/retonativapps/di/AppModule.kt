package com.example.santiago.retonativapps.di

import android.app.Application
import android.content.Context
import com.example.santiago.retonativapps.data.network.MoviesService
import com.example.santiago.retonativapps.data.network.utils.API_URL
import com.example.santiago.retonativapps.domain.IO_THREAD_SCHEDULER
import com.example.santiago.retonativapps.domain.MAIN_THREAD_SCHEDULER
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton

@Module
class AppModule {

    @Singleton
    @Provides
    fun provideContext(application: Application): Context = application

    @Singleton
    @Provides
    fun provideApiClient(): MoviesService {
        val clientBuilder = OkHttpClient.Builder()
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        clientBuilder.addInterceptor(loggingInterceptor)
        val retrofit = Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(API_URL)
            .client(clientBuilder.build())
            .build()
        return retrofit.create(MoviesService::class.java)
    }

    @Provides
    @Named(IO_THREAD_SCHEDULER)
    fun provideIOScheduler(): Scheduler = Schedulers.io()

    @Provides
    @Named(MAIN_THREAD_SCHEDULER)
    fun provideMainThreadScheduler(): Scheduler = AndroidSchedulers.mainThread()

}
