package com.example.santiago.retonativapps.di

import com.example.santiago.retonativapps.domain.IO_THREAD_SCHEDULER
import com.example.santiago.retonativapps.domain.MAIN_THREAD_SCHEDULER
import com.example.santiago.retonativapps.domain.interactors.base.GetMovieInteractor
import com.example.santiago.retonativapps.domain.interactors.base.ObservableUseCase
import com.example.santiago.retonativapps.domain.models.Movie
import com.example.santiago.retonativapps.domain.repositories.IMoviesRepository
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import javax.inject.Named
import javax.inject.Singleton

@Module
class InteractorsModule {

    @Singleton
    @Provides
    fun provideGetMovieInteractor(
        @Named(IO_THREAD_SCHEDULER) subscribeOnScheduler: Scheduler,
        @Named(MAIN_THREAD_SCHEDULER) observerOnScheduler: Scheduler,
        repository: IMoviesRepository
    ): ObservableUseCase<List<Movie>, String> {
        return GetMovieInteractor(subscribeOnScheduler, observerOnScheduler, repository)
    }

}
