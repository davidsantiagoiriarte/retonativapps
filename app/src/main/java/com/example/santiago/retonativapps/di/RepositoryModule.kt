package com.example.santiago.retonativapps.di

import com.example.santiago.retonativapps.data.network.MoviesService
import com.example.santiago.retonativapps.data.repositories.MoviesRepository
import com.example.santiago.retonativapps.domain.repositories.IMoviesRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {
    @Singleton
    @Provides
    fun provideMovieRepository(mApiService: MoviesService): IMoviesRepository {
        return MoviesRepository(mApiService)
    }
}
