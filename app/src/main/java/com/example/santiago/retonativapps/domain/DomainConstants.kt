package com.example.santiago.retonativapps.domain

const val IO_THREAD_SCHEDULER = "io_thread"
const val MAIN_THREAD_SCHEDULER = "main_thread"
