package com.example.santiago.retonativapps.domain.interactors.base

import io.reactivex.disposables.Disposable

interface IUseCase<R, P> {

    fun execute(
        params: P, onSuccessSubscriber: (response: R) -> Unit,
        onErrorSubscriber: (error: Throwable) -> Unit,
        doFinally: () -> Unit = {}
    ): Disposable
}