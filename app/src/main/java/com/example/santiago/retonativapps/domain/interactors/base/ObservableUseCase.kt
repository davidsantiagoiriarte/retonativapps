package com.example.santiago.retonativapps.domain.interactors.base

import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.disposables.Disposable

abstract class ObservableUseCase<R, P>(
    private val mSubscribeOnScheduler: Scheduler,
    private val mObserverOnSchedulers: Scheduler
) :
    IUseCase<R, P> {

    var cachedResponse: R? = null

    open var isCachingResponse: Boolean = false

    abstract fun buildUseCase(params: P): Observable<R>

    override fun execute(
        params: P,
        onSuccessSubscriber: (response: R) -> Unit,
        onErrorSubscriber: (error: Throwable) -> Unit,
        doFinally: () -> Unit
    ): Disposable {
        return buildUseCase(params)
            .subscribeOn(mSubscribeOnScheduler)
            .observeOn(mObserverOnSchedulers)
            .doFinally(doFinally)
            .subscribe({
                if (isCachingResponse) {
                    cachedResponse = it
                }
                onSuccessSubscriber(it)
            }, {
                when {
                    cachedResponse != null && isCachingResponse -> onSuccessSubscriber(
                        cachedResponse!!
                    )
                    else -> onErrorSubscriber(it)
                }
            })
    }
}