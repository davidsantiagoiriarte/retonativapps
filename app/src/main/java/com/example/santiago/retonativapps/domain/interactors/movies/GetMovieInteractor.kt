package com.example.santiago.retonativapps.domain.interactors.base

import com.example.santiago.retonativapps.domain.models.Movie
import com.example.santiago.retonativapps.domain.repositories.IMoviesRepository
import io.reactivex.Observable
import io.reactivex.Scheduler

class GetMovieInteractor(
    mSubscribeOnScheduler: Scheduler,
    mObserverOnScheduler: Scheduler,
    private val mRepository: IMoviesRepository
) : ObservableUseCase<List<Movie>, String>(mSubscribeOnScheduler, mObserverOnScheduler) {

    override fun buildUseCase(params: String): Observable<List<Movie>> {
        return mRepository.getMovies(params)
    }

}