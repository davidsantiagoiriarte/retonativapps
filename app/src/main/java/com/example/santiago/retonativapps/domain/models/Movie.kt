package com.example.santiago.retonativapps.domain.models

import com.example.santiago.retonativapps.util.EMPTY

data class Movie(
    val title: String?,
    val year: String?,
    val imdbID: String?,
    val type: String?,
    val poster: String?
) {
    override fun toString(): String {
        return title ?: EMPTY
    }
}
