package com.example.santiago.retonativapps.domain.repositories

import com.example.santiago.retonativapps.domain.models.Movie
import io.reactivex.Observable

interface IMoviesRepository {

    fun getMovies(search: String): Observable<List<Movie>>
}
