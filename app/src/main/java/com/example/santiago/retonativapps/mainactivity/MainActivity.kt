package com.example.santiago.retonativapps.mainactivity

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.ArrayAdapter
import com.example.santiago.retonativapps.R
import com.example.santiago.retonativapps.domain.models.Movie
import com.example.santiago.retonativapps.util.EMPTY
import com.example.santiago.retonativapps.util.loadImageFromUrl
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject


class MainActivity : DaggerAppCompatActivity(), MainContract.View {

    @Inject
    lateinit var mPresenter: MainContract.Presenter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mPresenter?.bind(this)
        etSearch?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                if (s.length > 0) {
                    mPresenter?.loadMovies(etSearch?.text.toString())
                }
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

            override fun afterTextChanged(s: Editable) {

            }
        })
        etSearch?.setOnItemClickListener { parent, view, position, id ->
            mPresenter?.selectMovie(position)
        }
    }

    override fun loadSearchResults(movies: ArrayList<Movie>) {
        etSearch?.setAdapter(
            ArrayAdapter<Movie>(
                this,
                android.R.layout.simple_dropdown_item_1line, movies
            )
        )
    }

    override fun showLoading(loading: Boolean) {
        pbSearch?.visibility = if (loading) View.VISIBLE else View.GONE
    }

    override fun showMovie(movie: Movie?) {
        if (movie == null) {
            ivPoster?.loadImageFromUrl(null)
            tvTitle?.text = EMPTY
            tvYear?.text = EMPTY
        } else {
            ivPoster?.loadImageFromUrl(movie.poster)
            tvTitle?.text = movie.title
            tvYear?.text = "Year ${movie.year}"
        }
    }

    override fun showMessage(message: String?) {
        message?.let {
            Snackbar.make(clMain, it, Snackbar.LENGTH_LONG)
                .show()
        }
    }

}
