package com.example.santiago.retonativapps.mainactivity

import com.example.santiago.retonativapps.domain.interactors.base.ObservableUseCase
import com.example.santiago.retonativapps.domain.models.Movie
import dagger.Module
import dagger.Provides

@Module
class MainActivityModule {

    @Provides
    fun provideMainPresenter(getMoviesInteractor: ObservableUseCase<List<Movie>, String>): MainContract.Presenter {
        return MainPresenter(getMoviesInteractor)
    }

}
