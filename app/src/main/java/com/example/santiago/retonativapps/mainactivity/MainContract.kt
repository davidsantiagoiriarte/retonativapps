package com.example.santiago.retonativapps.mainactivity

import com.example.santiago.retonativapps.domain.models.Movie

interface MainContract {

    interface View {
        fun loadSearchResults(movies: ArrayList<Movie>)
        fun showMessage(message: String?)
        fun showMovie(movie : Movie?)
        fun showLoading(loading : Boolean)
    }

    interface Presenter {
        fun bind(view: View)
        fun loadMovies(search: String)
        fun selectMovie(position : Int)
    }

}
