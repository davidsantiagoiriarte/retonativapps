package com.example.santiago.retonativapps.mainactivity

import com.example.santiago.retonativapps.domain.interactors.base.ObservableUseCase
import com.example.santiago.retonativapps.domain.models.Movie

class MainPresenter(private val mGetMoviesInteractor: ObservableUseCase<List<Movie>, String>) : MainContract.Presenter {

    private var mView: MainContract.View? = null

    private var mMovies: ArrayList<Movie>? = null

    override fun bind(view: MainContract.View) {
        mView = view
    }

    override fun loadMovies(search: String) {
        mView?.showLoading(true)
        mGetMoviesInteractor.execute(search, {
            mMovies = it as ArrayList<Movie>
            mView?.loadSearchResults(it)
            mView?.showLoading(false)
        }, {
            mView?.showLoading(false)
        })
    }

    override fun selectMovie(position: Int) {
        mMovies?.get(position)?.let {
            mView?.showMovie(it)
        }
    }

}
